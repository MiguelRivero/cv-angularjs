# Miguel Rivero CV - 2014#

I built this site based on [afriggeri cv](https://github.com/afriggeri/cv) using the following technologies:

* AngularJS
* HTML5
* LESS
* jQuery UI
* Bootstrap
* JSON
