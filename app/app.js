var Rivero_CV = angular.module('Rivero_CV',[
    'ngRoute',
    'pascalprecht.translate',
    'Rivero_CV_Controllers',
    'Rivero_CV_Services'
    ]);
Rivero_CV.config(['$routeProvider', '$translateProvider', '$locationProvider', '$compileProvider',
  function($routeProvider, $translateProvider, $locationProvider, $compileProvider){
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|mailto|tel|skype):/); // https?|ftp|file|
    $routeProvider.
        when('/en/',{
            controller:'showInfoCtrl',
            templateUrl:'app/views/tpl.html'
        }).
        when('/es/',{
            controller:'showInfoCtrl',
            templateUrl:'app/views/tpl.html'
        }).
        otherwise({
            redirectTo:'/en/'
        });
    $translateProvider.translations('en', {
        'CONTACT': 'contact',
        'EDUCATION': 'education',
        'EMPLOYMENT': 'experience',
        'LAN': 'languages',
        'PROGRAMMING': 'programming',
        'INTERESTS': 'interests',
        'PRO_SKILLS': 'professional skills',
        'PER_SKILLS': 'personal skills',
        'HONORS_AWARDS': 'honors & awards',
        // professional skills
        'LANGUAGES': "Languages",
        'DBMS': "DBMS",
        'IDE': "IDE",
        'OS': "OS",
        'DESIGN': "Design",
        'OTHERS': "Others",
        'HOURS': 'hours',
        // footer
        'BUILD': 'Built with',
        'BASED': 'Based on'
    })
    .translations('es', {
        'CONTACT': 'contacto',
        'LAN': 'idiomas',
        'PROGRAMMING': 'programación',
        'INTERESTS': 'intereses',
        'EDUCATION': 'formación',
        'EMPLOYMENT': 'experiencia',
        'PRO_SKILLS': 'tecnologías',
        'PER_SKILLS': 'aptitudes',
        'HONORS_AWARDS': 'premios y menciones',
        // professional skills
        'LANGUAGES': "Lenguajes",
        'DBMS': "BBDD",
        'IDE': "IDE",
        'OS': "SSOO",
        'DESIGN': "Diseño",
        'OTHERS': "Otros",
        'HOURS': 'horas',
        // footer
        'BUILD': 'Hecho con',
        'BASED': 'Basado en'
    });

    // if(window.history && window.history.pushState){
    //     $locationProvider.html5Mode(true);
    // }
}]);