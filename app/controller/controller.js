var Rivero_CV_Controllers = angular.module('Rivero_CV_Controllers', [ 'ngRoute', 'pascalprecht.translate' ]);

Rivero_CV_Controllers.controller('showInfoCtrl',['$scope', '$location', '$translate','Informacion', function($scope, $location, $translate, Informacion){
    $scope.datos = {};
    //console.log("location");
    // console.log($location.path());
    function getDatos() {
        $scope.$emit('LOAD')
        if ($location.path()=="/en/"){
            Informacion.getEnglishJSON()
                .success(function(data) {
                            $scope.datos = data;
                            $scope.$emit('UNLOAD');
                            console.info("The file en.json loaded succesfully");
                            $translate.use('en');
                            $scope.language='English';
                            countVisit($scope.language,$scope);
                            //contador('English');
                            //console.log($scope.datos);
                })
                .error(function(data, status, headers, config) {
                            console.error('Error: '+status);
                            console.error('Error message: '+ data.error.message);
                            console.error("There were error loading en.json")
                });
        }
        else if ($location.path()=="/es/"){
            Informacion.getSpanishJSON()
                .success(function(data) {
                            $scope.datos = data;
                            $scope.$emit('UNLOAD');
                            console.info("El archivo es.json se han recogido correctamente");
                            $translate.use('es');
                            $scope.language='Spanish';
                            countVisit($scope.language);
                            //contador('Spanish');
                            //console.log($scope.datos);
                })
                .error(function(data, status, headers, config) {
                            console.error('Error: '+status);
                            console.error('Mensaje del Error: '+ data.error.message);
                            console.error("El archivo es.json produjo errores")
                });
        }
    }

    getDatos();
    function color() {
        var colors = ["#6CE0F1", "#FB4485", "#FDA333", "#C2E15F", "#D3A4F9", "#EDD30E"];
        var entry = $(".principal h2");
        var i = 0;
        entry.each(function(){
            var title = $(this).text();
            var tit= title.substring(0, 3)
            var newString = "<span style=\"color:"+ colors[i] +"\">";
            newString += tit;
            newString += "</span>";
            newString += title.substring(3);
            $(this).html(newString);
            i++;
        });
    }

    function countVisit(lan, $scope) {
        localStorage[lan] = localStorage[lan] || 0;
        localStorage[lan] = +1 + parseInt(localStorage[lan]) ;
        var numberVisits = parseInt(localStorage[lan]);
        if ( (numberVisits >= 5) && (numberVisits%2 == 1)){
            if (lan=='English'){
                var modal=$("<div id=\"dialog-confirm\"> <p> <i class=\"fa fa-question fa-4x pull-right \" ></i>You visited my CV "+localStorage[lan]+" times.<br/>Do you want to hire me?<br/>RMiguelRivero@gmail.com</p></div>");
                var title="Hire Me";
                var cancel="Cancel";
                var contact="Contact me";
            }else if (lan=='Spanish'){
                var modal=$("<div id=\"dialog-confirm\"> <p> <i class=\"fa fa-question fa-4x pull-right \" ></i>Has visitado mi CV "+localStorage[lan]+" veces.<br/>¿Quieres contratarme?<br/>RMiguelRivero@gmail.com</p></div>");
                var title="Contratame";
                var cancel="Cancelar";
                var contact="Contactar";
            }
            modal.dialog({
                buttons: [{
                        text: cancel,
                        click: function(){
                            $( this ).dialog( "close" );
                            setTimeout(function(){
                                $( "#dialog-confirm" ).dialog( "destroy" ).remove();
                                $(".ui-effects-wrapper").remove();
                            }, 500);
                        }
                    },
                    {
                        text: contact,
                        click: function() {
                            document.location.href = "mailto:rmiguelrivero@gmail.com?subject=Job%20opportunity";
                            $( this ).dialog( "close" );
                            setTimeout(function(){
                                $( "#dialog-confirm" ).dialog( "destroy" ).remove();
                                $(".ui-effects-wrapper").remove();
                            }, 500);
                        }
                    }
                ],
                hide: { effect: "blind", duration: 500 },
                closeOnEscape: false,
                closeText: "hide",
                draggable: true,
                minHeight: 200,
                minWidth: 400,
                modal: true,
                resizable: false,
                title: title
            });
            $(".ui-dialog-titlebar-close").hide();
        }
    }
    color();

}])
.controller('loadCtrl',['$scope', function($scope){
    $scope.$on('LOAD', function(){
        $scope.loading=true;
    })
    $scope.$on('UNLOAD', function(){
        $scope.loading=false;
    })
}]);
