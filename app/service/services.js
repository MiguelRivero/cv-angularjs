var Rivero_CV_Services = angular.module('Rivero_CV_Services', []);

Rivero_CV_Services.service('Informacion', ['$http',function($http){

    this.getEnglishJSON = function(){
        return $http({
            method: 'GET',
            url: 'json/en.json',
            cache: true
        });
    }

    this.getSpanishJSON = function(){
        return $http({
            method: 'GET',
            url: 'json/es.json',
            cache: true
        });
    }

}]);